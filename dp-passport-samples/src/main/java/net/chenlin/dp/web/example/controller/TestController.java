package net.chenlin.dp.web.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 测试controller
 * @author zcl<yczclcn@163.com>
 */
@Controller
public class TestController {

    /**
     * 首页
     * @param model
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("test", "dp passport sample access success.");
        return "index.html";
    }

    /**
     * 匿名访问
     * @return
     */
    @RequestMapping(value = "/test/anonAccess", method = RequestMethod.GET)
    public String anon() {
        return "anon.html";
    }

    /**
     * 授权访问
     * @return
     */
    @RequestMapping(value = "/test/authAccess", method = RequestMethod.GET)
    public String auth() {
        return "auth.html";
    }

    /**
     * 路由访问
     * @return
     */
    @RequestMapping(value = "/test/routerAccess", method = RequestMethod.GET)
    public String router() {
        return "router.html";
    }

    /**
     * ajax异步请求
     * @return
     */
    @RequestMapping(value = "/test/ajaxAccess", method = RequestMethod.GET)
    @ResponseBody
    public String testAjax() {
        return "Success, This result is come from sample ajaxAccess!";
    }

}
