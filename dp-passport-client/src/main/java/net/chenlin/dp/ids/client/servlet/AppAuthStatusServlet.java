package net.chenlin.dp.ids.client.servlet;

import net.chenlin.dp.ids.client.manager.AuthCheckManager;
import net.chenlin.dp.ids.common.base.BaseResult;
import net.chenlin.dp.ids.common.constant.GlobalErrorEnum;
import net.chenlin.dp.ids.common.entity.SessionData;
import net.chenlin.dp.ids.common.util.JsonUtil;
import net.chenlin.dp.ids.common.util.WebUtil;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * app用户登录状态校验servlet
 * @author zcl<yczclcn@163.com>
 */
public class AppAuthStatusServlet extends HttpServlet {

    private AuthCheckManager authCheckManager;

    private static final long serialVersionUID = 8700620723419494542L;

    public AppAuthStatusServlet(AuthCheckManager authCheckManager) {
        this.authCheckManager = authCheckManager;
    }

    /**
     * get请求
     * @param req
     * @param resp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        SessionData sessionData = authCheckManager.checkAppSession(req);
        if (sessionData == null) {
            BaseResult notLoginRes = GlobalErrorEnum.NOT_LOGIN.getResult();
            String json = JsonUtil.toStr(notLoginRes);
            WebUtil.write(resp, json);
            return;
        }
        BaseResult hasLoginRes = GlobalErrorEnum.HAS_LOGIN.getResult();
        hasLoginRes.setRespData(sessionData);
        String json = JsonUtil.toStr(hasLoginRes);
        WebUtil.write(resp, json);
    }

    /**
     * post请求
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        this.doGet(req, resp);
    }

}
