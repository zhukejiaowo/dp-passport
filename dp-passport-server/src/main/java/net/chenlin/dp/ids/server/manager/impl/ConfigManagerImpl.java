package net.chenlin.dp.ids.server.manager.impl;

import net.chenlin.dp.ids.common.constant.IdsConst;
import net.chenlin.dp.ids.common.util.CommonUtil;
import net.chenlin.dp.ids.common.util.JsonUtil;
import net.chenlin.dp.ids.server.dao.PassportConfigMapper;
import net.chenlin.dp.ids.server.entity.PassportConfigDO;
import net.chenlin.dp.ids.server.manager.ConfigManager;
import net.chenlin.dp.ids.server.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 配置管理模块
 * @author zhouchenglin[yczclcn@163.com]
 */
@Service("configManager")
public class ConfigManagerImpl implements ConfigManager {

    @Autowired
    private PassportConfigMapper configMapper;

    /**
     * 获取配置项值
     * @param key
     * @return
     */
    @Override
    public String getConfig(String key) {
        String json = RedisUtil.get(key);
        PassportConfigDO configDO;
        if (CommonUtil.strIsEmpty(json)) {
            configDO = configMapper.getByKey(key);
            json = JsonUtil.toStr(configDO);
            RedisUtil.set(key, json, IdsConst.REDIS_EXPIRE_TIME);
            return configDO.getMacroValue();
        }
        configDO = JsonUtil.toObj(json, PassportConfigDO.class);
        if (configDO != null) {
            return configDO.getMacroValue();
        }
        return null;
    }

}
