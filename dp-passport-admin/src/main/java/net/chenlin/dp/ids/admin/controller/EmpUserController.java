package net.chenlin.dp.ids.admin.controller;

import net.chenlin.dp.ids.admin.core.base.BasePage;
import net.chenlin.dp.ids.admin.service.EmpUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * passport用户controller
 * @author zhouchenglin[yczclcn@163.com]
 */
@Controller
@RequestMapping("/user")
public class EmpUserController {

    @Autowired
    private EmpUserService empUserService;

    /**
     * 列表页
     * @return
     */
    @RequestMapping
    public String index() {
        return "base/user/list.html";
    }

    /**
     * 分页查询
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public Object list() {
        return new BasePage<>(empUserService.listPage(null));
    }

}
